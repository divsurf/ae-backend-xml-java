package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JsoupFindMakeButton {

    private static Logger LOGGER = LoggerFactory.getLogger(JsoupFindMakeButton.class);

    private static String CHARSET_NAME = "utf8";

    private static List<String> TARGET_CSS_CLASSES = Arrays.asList("btn-success", "test-link-ok");

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            throw new RuntimeException(
                    "java -jar <your_bundled_app>.jar " +
                            "<input_origin_file_path> <input_other_sample_file_path> <target_element_id>");
        }
        String inputOriginFilePath = args[0];
        String otherSampleFilePath = args[1];
        String targetElementId = args[2];

        List<File> originalFiles = Files.walk(Paths.get(inputOriginFilePath))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        List<File> otherSampleFiles = Files.walk(Paths.get(otherSampleFilePath))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        List<File> allFiles = new ArrayList(originalFiles);
        allFiles.addAll(otherSampleFiles);

        for (File file : allFiles) {
            Optional<Elements> elementsOpt = findElements(file, targetElementId);

            if (!elementsOpt.get().isEmpty()) {
                LOGGER.info(file.getAbsolutePath() + " -> " + elementsOpt.get().get(0).childNodes().get(0).toString());
            }

            Optional<List<String>> elementsAttrsOpts = elementsOpt.map(buttons ->
                    {
                        List<String> stringifiedAttrs = new ArrayList<>();

                        buttons.iterator().forEachRemaining(button ->
                                stringifiedAttrs.add(
                                        button.attributes().asList().stream()
                                                .map(attr -> attr.getKey() + " = " + attr.getValue())
                                                .collect(Collectors.joining(", "))));

                        return stringifiedAttrs;
                    }
            );


            elementsAttrsOpts.ifPresent(attrsList ->
                    attrsList.forEach(attrs ->
                            LOGGER.info("Target element attrs: [{}]", attrs)
                    )
            );
        }
    }

    private static Optional<Elements> findElements(File htmlFile, String targetElementId) {
        Optional<Elements> result = Optional.empty();

        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            Optional<Element> foundElementOpt = Optional.ofNullable(doc.getElementById(targetElementId));

            if (foundElementOpt.isPresent()) {
                result = Optional.of(new Elements(foundElementOpt.get()));
            } else {
                for (String targetCssClass : TARGET_CSS_CLASSES) {
                    Optional<Elements> foundElementsOpt = Optional.ofNullable(doc.getElementsByClass(targetCssClass));
                    if (!foundElementsOpt.isPresent() || foundElementsOpt.get().isEmpty()) {
                        continue;
                    }
                    result = foundElementsOpt;
                }

                for (Element element : result.get()) {
                    if (!element.attributes().get("title").endsWith("Make-Button")) {
                        continue;
                    }

                    result = Optional.of(new Elements(element));
                }
            }

            return result;

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

}
