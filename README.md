# AgileEngine backend-XML java

**Path to JAR:**

ae-backend-xml-java \ build \ **distributions**

java -jar <your_bundled_app>.jar <input_origin_file_path> <input_other_sample_file_path> <target_element_id>

_For example:_                            
java -jar ae-backend-xml-java-0.0.1.jar C:\input_origin_file_path\ C:\input_other_sample_file_path\ make-everything-ok-button


**Results:**

[INFO] 2019-09-30 19:53:44,574 c.a.JsoupFindMakeButton - C:\input_origin_file_path\sample-0-origin.html ->  Make everything OK

[INFO] 2019-09-30 19:53:44,619 c.a.JsoupFindMakeButton - Target element attrs: [id = make-everything-ok-button, class = btn btn-success, href = #ok, title = M
ake-Button, rel = next, onclick = javascript:window.okDone(); return false;]


[INFO] 2019-09-30 19:53:44,632 c.a.JsoupFindMakeButton - C:\input_other_sample_file_path\sample-1-evil-gemini.html ->  Make everything OK

[INFO] 2019-09-30 19:53:44,632 c.a.JsoupFindMakeButton - Target element attrs: [class = btn btn-success, href = #check-and-ok, title = Make-Button, rel = done
, onclick = javascript:window.okDone(); return false;]


[INFO] 2019-09-30 19:53:44,641 c.a.JsoupFindMakeButton - C:\input_other_sample_file_path\sample-2-container-and-clone.html ->  Make everything OK

[INFO] 2019-09-30 19:53:44,642 c.a.JsoupFindMakeButton - Target element attrs: [class = btn test-link-ok, href = #ok, title = Make-Button, rel = next, onclick
 = javascript:window.okComplete(); return false;]


[INFO] 2019-09-30 19:53:44,655 c.a.JsoupFindMakeButton - C:\input_other_sample_file_path\sample-3-the-escape.html ->  Do anything perfect

[INFO] 2019-09-30 19:53:44,661 c.a.JsoupFindMakeButton - Target element attrs: [class = btn btn-success, href = #ok, title = Do-Link, rel = next, onclick = ja
vascript:window.okDone(); return false;]


[INFO] 2019-09-30 19:53:44,676 c.a.JsoupFindMakeButton - C:\input_other_sample_file_path\sample-4-the-mash.html ->  Do all GREAT

[INFO] 2019-09-30 19:53:44,681 c.a.JsoupFindMakeButton - Target element attrs: [class = btn btn-success, href = #ok, title = Make-Button, rel = next, onclick
= javascript:window.okFinalize(); return false;]


It is built on top of [Jsoup](https://jsoup.org/).

You can use Jsoup for your solution or apply any other convenient library. 
